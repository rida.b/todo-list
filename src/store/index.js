import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    listActivity: [],
    status: false,
    status1: false,
    detailActivity: {},
    listItems: [],
    loadingCreate: false,
  },
  getters: {
    listActivity: (state) => {
      return state.listActivity;
    },
    status: (state) => {
      return state.status;
    },
    status1: (state) => {
      return state.status1;
    },
    detailActivity: (state) => {
      return state.detailActivity;
    },
    listItems: (state) => {
      return state.listItems;
    },
    loadingCreate: (state) => {
      return state.loadingCreate;
    },
  },
  mutations: {
    setListActivity: (state, payload) => {
      state.listActivity = payload;
    },
    setStatus: (state, payload) => {
      state.status = payload;
    },
    setStatus1: (state, payload) => {
      state.status1 = payload;
    },
    setDetailActivity: (state, payload) => {
      state.detailActivity = payload;
    },
    setListItems: (state, payload) => {
      state.listItems = payload;
    },
    setLoadingCreate: (state, payload) => {
      state.loadingCreate = payload;
    },
  },
  actions: {
    getListActivity: async ({ commit }) => {
      const params = new URLSearchParams();
      params.append("email", process.env.VUE_APP_EMAIL_BASE);
      try {
        const response = await axios.get(`${process.env.VUE_APP_API_BASE_URL}/activity-groups?${params}`);
        commit("setListActivity", response.data.data);
      } catch (error) {
        return error;
      }
    },
    postActivity: async ({ dispatch }) => {
      const params = {
        email: process.env.VUE_APP_EMAIL_BASE,
        title: "New Activity",
      };
      try {
        const response = await axios.post(`${process.env.VUE_APP_API_BASE_URL}/activity-groups`, params);
        dispatch("getListActivity");
        return response;
      } catch (error) {
        return error;
      }
    },
    deleteActivity: async ({ commit, dispatch }, payload) => {
      try {
        const response = await axios.delete(`${process.env.VUE_APP_API_BASE_URL}/activity-groups/${payload}`);
        if (response.status == 200) {
          commit("setStatus", true);
          dispatch("getListActivity");
          setTimeout(() => {
            commit("setStatus", false);
          }, 10000);
        }
      } catch (error) {
        return error;
      }
    },
    getDetailActivity: async ({ commit }, payload) => {
      try {
        const response = await axios.get(`${process.env.VUE_APP_API_BASE_URL}/activity-groups/${payload}`);
        commit("setDetailActivity", response.data);
        commit("setListItems", response.data.todo_items);
      } catch (error) {
        return error;
      }
    },
    updateActivity: async ({ dispatch }, payload) => {
      const params = {
        title: payload.title,
      };
      try {
        const response = await axios.patch(`${process.env.VUE_APP_API_BASE_URL}/activity-groups/${payload.id}`, params);
        if (response.status == 200) {
          dispatch("getDetailActivity", payload.id);
        }
      } catch (error) {
        return error;
      }
    },
    postCreateTodo: async ({ commit, dispatch }, payload) => {
      try {
        commit("setLoadingCreate", true);
        const response = await axios.post(`${process.env.VUE_APP_API_BASE_URL}/todo-items`, payload);
        if (response.status == 201) {
          dispatch("getDetailActivity", payload.activity_group_id);
          commit("setLoadingCreate", false);
        }
      } catch (error) {
        commit("setLoadingCreate", false);
        return error;
      }
    },
    postDeleteTodo: async ({ commit, dispatch }, payload) => {
      try {
        const response = await axios.delete(`${process.env.VUE_APP_API_BASE_URL}/todo-items/${payload.id}`);
        if (response.status == 200) {
          commit("setStatus1", true);
          dispatch("getDetailActivity", payload.activity_group_id);
          setTimeout(() => {
            commit("setStatus1", false);
          }, 10000);
        }
      } catch (error) {
        return error;
      }
    },
    updateTodo: async ({ dispatch }, payload) => {
      const params = {
        title: payload.title,
        priority: payload.priority,
        is_active: payload.is_active,
      };
      try {
        const response = await axios.patch(`${process.env.VUE_APP_API_BASE_URL}/todo-items/${payload.id}`, params);
        if (response.status == 200) {
          dispatch("getDetailActivity", payload.activity_group_id);
        }
      } catch (error) {
        return error;
      }
    },
    updateStatus: async ({ dispatch }, payload) => {
      const params = {
        priority: payload.priority,
        is_active: payload.is_active == 1 ? 0 : 1,
      };
      try {
        const response = await axios.patch(`${process.env.VUE_APP_API_BASE_URL}/todo-items/${payload.id}`, params);
        if (response.status == 200) {
          dispatch("getDetailActivity", payload.activity_group_id);
        }
      } catch (error) {
        return error;
      }
    },
  },
  modules: {},
});
